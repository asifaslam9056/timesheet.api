﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.model;

namespace timesheet.business.Interface
{
    public interface IEmpEffortDetailService
    {
            IQueryable<EmployeeTimeSheet> GetEmployeeSheetByEmpID(int empid);
            bool SaveTaskSheet(EmpEffortModel empmodel);
        
    }
}
