﻿using System;
using System.Linq;
using timesheet.business.Interface;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmpEffortDetailService : IEmpEffortDetailService
    {
        public TimesheetDb db { get; }
        public EmpEffortDetailService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<EmployeeTimeSheet> GetEmployeeSheetByEmpID(int empid)
        {
            return this.db.EmployeeTimeSheets.Where(t => t.EmployeeId == empid);
        }

        public bool SaveTaskSheet(EmpEffortModel empmodel)
        {
            if (empmodel != null)
            {
                this.db.EmployeeTimeSheets.Add(new EmployeeTimeSheet()
                {
                    EmployeeId = empmodel.EmployeeId,
                    TaskId = empmodel.TaskId,
                    WorkingDate = empmodel.WorkingDate,
                    WorkingHours = empmodel.WorkingHours
                });
                this.db.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
