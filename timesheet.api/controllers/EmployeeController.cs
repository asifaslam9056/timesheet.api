﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.api.models;
using timesheet.business;
using timesheet.business.Interface;

namespace timesheet.api.controllers
{
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService employeeService;
        private readonly IEmpEffortDetailService empEffortDetlSheetService;
        private readonly ITaskService taskService;
        public EmployeeController(IEmployeeService employeeService, IEmpEffortDetailService empEffortDetlSheetService, ITaskService taskService)
        {
            this.employeeService = employeeService;
            this.empEffortDetlSheetService = empEffortDetlSheetService;
            this.taskService = taskService;
        }
        //api/v1/employee/getAll

        [HttpGet("getAll")]
        public IActionResult GetAll()
        {
            int daysInWeek = -7;
            var startDate = DateTime.Today.Date.AddDays(daysInWeek);
            var endDate = DateTime.Today.Date;
            int avgHrs = 0;
            int totalWrkHrs = 0;
            var result = new List<EmployeeModel>();
            var employees = this.employeeService.GetEmployees();
            foreach (var emp in employees)
            {
                avgHrs = 0;
                totalWrkHrs = 0;
                var sheets = this.empEffortDetlSheetService.GetEmployeeSheetByEmpID(emp.Id).Where(t=>t.WorkingDate >= startDate && t.WorkingDate <= endDate);
                totalWrkHrs = sheets.Sum(t => t.WorkingHours);
                if (sheets != null && sheets.Count() > 0 && totalWrkHrs>0)
                {
                    avgHrs = totalWrkHrs / sheets.GroupBy(t => t.WorkingDate).Count();
                }

                result.Add(new EmployeeModel()
                {
                    Id = emp.Id,
                    Name = emp.Name,
                    Code = emp.Code,
                    WeeklyAvrage = avgHrs,
                    WeeklyTotal = totalWrkHrs
                });
            }
            return new ObjectResult(result);
            
        }

        [HttpGet("getAllTasks")]
        public IActionResult getalltasks()
        {
            var items = this.taskService.GetAllTasks();
            return new ObjectResult(items);
        }

        [HttpPost("saveEmployeeTaskSheet")]
        public IActionResult saveEmployeeTaskSheet(EmpEffortModel model)
        {
            return new ObjectResult(empEffortDetlSheetService.SaveTaskSheet(model));
        }

        [HttpGet("GetEmployeeTimeSheetByID/{id}")]
        public IActionResult GetEmployeeTimeSheetByID(int id)
        {
            int daysInWeek = -7;
            var startDate = DateTime.Today.Date.AddDays(daysInWeek);
            var endDate = DateTime.Today.Date;
            var result = new List<EmployeeTaskViewModel>();
            var sheets = this.empEffortDetlSheetService.GetEmployeeSheetByEmpID(id).Where(t=> t.WorkingDate >= startDate && t.WorkingDate <= endDate).GroupBy(t => t.Task);
            foreach (var task in sheets)
            {
                result.Add(new EmployeeTaskViewModel()
                {
                    Task = task.Key.Name,
                    MondayHours = task.Where(t => t.WorkingDate.DayOfWeek.ToString() == "Monday").Select(x => x.WorkingHours).Sum(),
                    TuesdayHours = task.Where(t => t.WorkingDate.DayOfWeek.ToString() == "Tuesday").Select(x => x.WorkingHours).Sum(),
                    WenesdayHours = task.Where(t => t.WorkingDate.DayOfWeek.ToString() == "Wednesday").Select(x => x.WorkingHours).Sum(),
                    ThursdayHours = task.Where(t => t.WorkingDate.DayOfWeek.ToString() == "Thursday").Select(x => x.WorkingHours).Sum(),
                    FridayHours = task.Where(t => t.WorkingDate.DayOfWeek.ToString() == "Friday").Select(x => x.WorkingHours).Sum(),
                    SaturdayHours = task.Where(t => t.WorkingDate.DayOfWeek.ToString() == "Saturday").Select(x => x.WorkingHours).Sum(),
                    SundayHours = task.Where(t => t.WorkingDate.Date.DayOfWeek.ToString() == "Sunday").Select(x => x.WorkingHours).Sum(),
                });
            }
            return new ObjectResult(result);
        }
    }
}